package controllers

import anorm._
import play.api.db.DB
import play.api._
import play.api.mvc._
import play.api.libs.json._
import play.api.Play.current
import models.{DrinkModel, UserModel}
import java.util.UUID
import utils.JsonWritesImplicits._

/**
 * Created by Michael on 13.08.2014.
 */
object Api extends Controller {

  def index = Action {

    val u: UserModel = UserModel(None, "H", "pw")
    UserModel.save(u)

    var d: DrinkModel = new DrinkModel("horni", 50)
    DrinkModel.save(d)
    d = new DrinkModel("edel-horni", 80)
    DrinkModel.save(d)

    Ok(Json.toJson(UserModel.findAllUsers))
  }

  def findAllUsers = Action {
    Ok(Json.toJson(UserModel.findAllUsers))
  }

  def drink(drinkType: String) = Action {
    Ok(drinkType)
  }

  def showDrinks() = Action {
    Ok(Json.toJson(DrinkModel.findAllDrinks))
  }

}