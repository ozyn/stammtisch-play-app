package models

import java.util.UUID
import AnormImplicits._
import anorm._
import anorm.SqlParser._
import play.api.Play.current
import play.api.db.DB
import com.google.common.hash.Hashing
import java.nio.charset.StandardCharsets

/**
 * Created by Michael on 13.08.2014.
 */
case class UserModel(val id: Option[UUID], val username: String, val password: String) {
}

object UserModel {

  private val rowParser: RowParser[UserModel] = {
    get[UUID]("id") ~
      get[String]("username") ~
      get[String]("password") map {
      case id ~ username ~ password=> {
        UserModel(Some(id), username, password)
      }
    }
  }
  // private val SECRET_SALT = "epoqictp4zmqcptiqpi"

  // password = Hashing.sha256().hashString(password + SECRET_SALT, StandardCharsets.UTF_8).toString

  // var id: Long = -1
  // var uuid: UUID = new UUID(0, 0)
  // var isNew: Boolean = true

  def save(model:UserModel) = {
    val uuid = model.id match {
      case None => getSafeUuid
      case Some(uuid) => uuid
    }

    DB.withConnection(implicit connection => {
      SQL(
        """
          |INSERT INTO User(id, username, password)
          |VALUES ({id}, {username}, {password})
          |ON DUPLICATE KEY UPDATE
          |  username = VALUES(username),
          |  password = VALUES(password)
        """.stripMargin)
        .on('id -> uuid, 'username -> model.username, 'password -> model.password)
        .executeInsert();
    })

    //return UserModel(uuid, model.username, model.password)
  }

  def findUserById(userId: UUID): Option[UserModel] = {
    DB.withConnection(implicit connection => {
      SQL(
        """
          |SELECT * FROM User
          |WHERE id = {id}
        """.stripMargin)
        .on('id -> userId)
        .as(rowParser.singleOpt)(connection)
    })
  }

  def findAllUsers: List[UserModel] = {
    SQL(
      """
        |SELECT * FROM User
      """.stripMargin
    ).as(rowParser *)(DB.getConnection())
  }

  private def getSafeUuid : UUID = {
    val uuid = UUID.randomUUID
    val user = findUserById(uuid)

    user match {
      case None => uuid
      case Some(_) => getSafeUuid
    }
  }
}
