package models

import AnormImplicits._
import anorm._
import anorm.SqlParser._
import play.api.Play.current
import java.util.UUID
import play.api.db.DB
import anorm.~

/**
 * Created by Michael on 15.08.2014.
 */
case class DrinkModel(val name: String, val price: Int) {
}

object DrinkModel {

  private val rowParser: RowParser[DrinkModel] = {
    get[String]("name") ~
      get[Int]("price") map {
      case name ~ price => {
        new DrinkModel(name, price)
      }
    }
  }

  def save(model: DrinkModel) = {
    DB.withConnection(implicit connection => {
      SQL(
        """
          |INSERT INTO Drink(name, price)
          |VALUES ({name}, {price})
          |ON DUPLICATE KEY UPDATE
          |  price = VALUES(price)
        """.stripMargin)
        .on('name -> model.name, 'price -> model.price)
        .executeUpdate()
    })
  }

  def findDrinkByName(name: String): Option[DrinkModel] = {
    DB.withConnection(implicit connection => {
      SQL(
        """
          |SELECT * FROM Drink
          |WHERE name = {name}
        """.stripMargin)
        .on('name -> name)
        .as(rowParser.singleOpt)(connection)
    })
  }

  def findAllDrinks: List[DrinkModel] = {
    SQL(
      """
        |SELECT * FROM Drink
      """.stripMargin
    ).as(rowParser *)(DB.getConnection())
  }
}
