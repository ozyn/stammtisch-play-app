package models

import java.sql.Blob
import java.sql.Clob
import java.util.UUID
import utils.UUIDHelper
import anorm._
import com.google.common.io.ByteStreams

object AnormImplicits {
  /**
   * Attempt to convert a SQL value into a byte array
   *
   * @param value value to convert
   * @return byte array
   */
  private def valueToByteArrayOption(value: Any): Option[Array[Byte]] = {
    try {
      value match {
        case bytes: Array[Byte] => Some(bytes)
        case clob: Clob => Some(ByteStreams.toByteArray(clob.getAsciiStream()))
        case blob: Blob => Some(blob.getBytes(1, blob.length.asInstanceOf[Int]))
        case _ => None
      }
    }
    catch {
      case e: Exception => None
    }
  }

  /**
   * Attempt to convert a SQL value into a UUID
   *
   * @param value value to convert
   * @return UUID
   */
  private def valueToUUIDOption(value: Any): Option[UUID] = {
    try {
      valueToByteArrayOption(value) match {
        case Some(bytes) => Some(UUIDHelper.fromByteArray(bytes))
        case _ => None
      }
    }
    catch {
      case e: Exception => None
    }
  }

  /**
   * Implicit conversion from anorm row to byte array
   */
  implicit def rowToByteArray: Column[Array[Byte]] = {
    Column.nonNull[Array[Byte]] { (value, meta) =>
      val MetaDataItem(qualified, nullable, clazz) = meta
      valueToByteArrayOption(value) match {
        case Some(bytes) => Right(bytes)
        case _ => Left(TypeDoesNotMatch("Cannot convert " + value + ":" + value.asInstanceOf[AnyRef].getClass + " to Byte Array for column " + qualified))
      }
    }
  }

  /**
   * Implicit converstion from anorm row to uuid
   */
  implicit def rowToUUID: Column[UUID] = {
    Column.nonNull[UUID] { (value, meta) =>
      val MetaDataItem(qualified, nullable, clazz) = meta
      valueToUUIDOption(value) match {
        case Some(uuid) => Right(uuid)
        case _ => Left(TypeDoesNotMatch("Cannot convert " + value + ":" + value.asInstanceOf[AnyRef].getClass + " to UUID for column " + qualified))
      }
    }
  }

  /**
   * Implicit conversion from UUID to anorm statement value
   */
  implicit def uuidToStatement = new ToStatement[UUID] {
    def set(s: java.sql.PreparedStatement, index: Int, aValue: UUID): Unit = s.setObject(index, UUIDHelper.toByteArray(aValue))
  }
}