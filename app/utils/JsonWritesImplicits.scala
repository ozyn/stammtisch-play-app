package utils

import play.api.libs.json._
import models.{DrinkModel, UserModel}

/**
 * Created by Michael on 15.08.2014.
 */
object JsonWritesImplicits {
  implicit val placeWrites = new Writes[UserModel] {
    def writes(user: UserModel) = Json.obj(
      "id" -> user.id,
      "username" -> user.username,
      "password" -> user.password
    )
  }

  implicit val drinksWrites = new Writes[DrinkModel] {
    def writes(drink: DrinkModel) = Json.obj(
      "name" -> drink.name,
      "price" -> drink.price
    )
  }
}
