# Users schema

# --- !Ups

CREATE TABLE User (
    id binary(16) NOT NULL,
    password varchar(255) NOT NULL,
    username varchar(255) NOT NULL,
    PRIMARY KEY (id)
);

CREATE TABLE Beer (
    id binary(16) NOT NULL,
    drinkingUserId bigint(20) NOT NULL,
    payingUserId bigint(20) NOT NULL,
    drankAtUnix bigint(20) NOT NULL,
    PRIMARY KEY (id)
);

CREATE TABLE Drink (
    name varchar(255) NOT NULL,
    price int NOT NULL,
    PRIMARY KEY (name)
);

# --- !Downs

DROP TABLE Drink;
DROP TABLE Beer;
DROP TABLE User;